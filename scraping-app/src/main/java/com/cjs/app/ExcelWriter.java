package com.cjs.app;

import javafx.collections.ObservableList;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.*;

public class ExcelWriter {

    // Get todays date and time and format it to yyyy-MM-dd_HH_mm to include in a unique file name
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH_mm");
    String dateFormatted = sdf.format(cal.getTime());

    String fileName = "FinanicalData_"+dateFormatted+".xlsx";
    private final String FILE_NAME = "/tmp/"+fileName;


    ObservableList<Overview> data;
    XSSFWorkbook workbook;
    XSSFSheet sheet;

    public ExcelWriter(ObservableList<Overview> _data) {
        this.data = _data;
        this.workbook = new XSSFWorkbook();
        this.sheet = workbook.createSheet("Financial data");
    }

    public void writeToFile() {

        int rowNum = 0;
        System.out.println("Creating excel");

        // Write header, Company, Direktavkastning, Eget Kapital, etc
        Row firstRow = sheet.createRow(rowNum++);
        int firstRowColNum = 0;
        Set keys = data.get(0).getDataDetail().keySet();
        Iterator iter = keys.iterator();
        System.out.println("KEYS: " + keys);
        Cell cell = firstRow.createCell(firstRowColNum++);
        cell.setCellValue("Company");
        while (iter.hasNext()){
            cell = firstRow.createCell(firstRowColNum++);
            cell.setCellValue((String) iter.next());
            //iter.remove(); // avoids a ConcurrentModificationException
        }

        // Write rest of the data to the spreadsheet
        for (Overview d : data) {
            Row row = sheet.createRow(rowNum++);
            int colNum = 0;
            Iterator it = d.getDataDetail().entrySet().iterator();
            String company = d.getName();
            cell = row.createCell(colNum++);
            cell.setCellValue(company);
            while (it.hasNext()) {
                cell = row.createCell(colNum++);
                Map.Entry pair = (Map.Entry) it.next();
                cell.setCellValue((double) pair.getValue());
                System.out.println(pair.getKey() + " = " + pair.getValue());
                //it.remove(); // avoids a ConcurrentModificationException
            }
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
            workbook.write(outputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Done");
    }

    String getFileName(){
        return fileName;
    }

    String getStored(){
        return FILE_NAME;
    }
}
