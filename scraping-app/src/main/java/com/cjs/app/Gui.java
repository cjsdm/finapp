package com.cjs.app;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.openqa.selenium.WebDriver;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import java.awt.event.ActionEvent;

import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class Gui extends Application {
    // Todo - Delete data from table - OK
    // Todo - Store financial data for access, exporting etc. - OK stored in ObservableList data
    // Todo - Export to MS excel - OK
    // Todo - Better error handling - OK but can be better
    // Todo - Click to see preview of data? - OK but only on command line, put in GUI console - OK
    // Todo - Add text area with necessary outputs such as "data details", where excel file is stored an its name etc. - OK

    int WIDTH_OF_WINDOW = 620;
    int HEIGHT_OF_WINDOW = 735;

    WebDriver driver;
    DriverController driverController;
    Scrape scrapeObj;
    private TableView<Overview> table = new TableView();

    private final ObservableList<Overview> data =
            FXCollections.observableArrayList(
                    //new Overview("kinnevik", "Kinnevik B", "Error loading data", null),
                    //new Overview("investor", "Investor B", "Error loading data", null)
            );

    Button deleteButton;
    Button infoButton;
    Button exportButton;
    TextArea console;

    //String consoleText = "";

    StringBuilder consoleText = new StringBuilder("Console...\n\n");

    final HBox hb = new HBox();
    final HBox hb_below = new HBox();
    final HBox hb_console = new HBox();

    @Override
    public void start(Stage stage) {
        // Set up web driver
        driverController = new DriverController();
        driverController.configureDriver();

        driver = driverController.getDriver();

        Scene scene = new Scene(new Group());
        stage.setTitle("Financial Data");
        stage.setWidth(WIDTH_OF_WINDOW);
        stage.setHeight(HEIGHT_OF_WINDOW);

        final Label label = new Label("Financial data");
        label.setFont(new Font("Arial", 20));

        table.setEditable(true);

        Callback<TableColumn, TableCell> stringCellFactory =
                new Callback<TableColumn, TableCell>() {
                    @Override
                    public TableCell call(TableColumn p) {
                        MyStringTableCell cell = new MyStringTableCell();
                        cell.addEventFilter(MouseEvent.MOUSE_CLICKED, new MyEventHandler());
                        return cell;
                    }
                };

        TableColumn identifierCol = new TableColumn("Identifier");
        identifierCol.setMinWidth(200);
        identifierCol.setCellValueFactory(
                new PropertyValueFactory<Overview, String>("identifier"));
        identifierCol.setCellFactory(stringCellFactory);


        TableColumn nameCol = new TableColumn("Name");
        nameCol.setMinWidth(200);
        nameCol.setCellValueFactory(
                new PropertyValueFactory<Overview, String>("name"));
        nameCol.setCellFactory(stringCellFactory);

        TableColumn statusCol = new TableColumn("Status");
        statusCol.setMinWidth(200);
        statusCol.setCellValueFactory(
                new PropertyValueFactory<Overview, String>("status"));
        statusCol.setCellFactory(stringCellFactory);

        table.setItems(data);
        table.getColumns().addAll(identifierCol, nameCol, statusCol);


        console = new TextArea();
        console.setMinWidth(600);
        console.setMinHeight(200);
        console.setEditable(false);
        console.setText(consoleText.toString());


        deleteButton = new Button("Delete");
        deleteButton.setDisable(true);
        deleteButton.setOnAction(e -> {
            Overview selectedItem = table.getSelectionModel().getSelectedItem();
            consoleText.append("****************\n" + selectedItem.getName() + " REMOVED\n****************\n");
            table.getItems().remove(selectedItem);
            console.setText(consoleText.toString());
        });

        infoButton = new Button("Show details");
        infoButton.setDisable(true);
        infoButton.setOnAction(e -> {
            Overview selectedItem = table.getSelectionModel().getSelectedItem();
            System.out.println(selectedItem.getDataDetail());
            if (selectedItem.getStatus() != "Error loading data") {
                Iterator it = selectedItem.getDataDetail().entrySet().iterator();
                System.out.println(selectedItem.getName());
                consoleText.append("----------------\n" + selectedItem.getName() + "\n");
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    consoleText.append(pair.getKey());
                    consoleText.append(": ");
                    consoleText.append(pair.getValue());
                    consoleText.append("\n");
                }
                consoleText.append("----------------\n");
                console.setText(consoleText.toString());
            }else {
                consoleText.append("Sorry, no data associated with this object\n");
                console.setText(consoleText.toString());
            }

        });

        exportButton = new Button("Export to .xlsx");
        exportButton.setOnAction(e -> {
            if (!data.isEmpty()) {
                ExcelWriter excelWriter = new ExcelWriter(data);
                excelWriter.writeToFile();
                // TODO - create method/class for exporting all data in "data" to MS Excel.
                Overview selectedItem = table.getSelectionModel().getSelectedItem();
                System.out.println(selectedItem.getDataDetail());
                consoleText.append("File name: ");
                consoleText.append(excelWriter.getFileName());
                consoleText.append("\n");
                consoleText.append("Stored under: ");
                consoleText.append(excelWriter.getStored());
                consoleText.append("\n");
                console.setText(consoleText.toString());

            }else{
                consoleText.append("No data to export, please load data first\n");
                console.setText(consoleText.toString());
                System.out.println("No data to export");
            }

        });


        final TextField addIdentifier = new TextField();
        //addIdentifier.setPrefColumnCount(20);
        addIdentifier.setPromptText("stock identifier");
        addIdentifier.setMaxWidth(identifierCol.getPrefWidth() + nameCol.getPrefWidth() + statusCol.getPrefWidth());
        addIdentifier.setOnMouseClicked(e -> {
            deleteButton.setDisable(true);
            infoButton.setDisable(true);
        });


        final Button loadButton = new Button("Load");
        loadButton.setOnAction((event) -> {
            // Boolean as generic parameter since you want to return it
            Task<Boolean> task = new Task<Boolean>() {
                @Override
                public Boolean call() {
                    String currentFirm = addIdentifier.getText();
                    System.out.println("Key: " + currentFirm);
                    // do your operation in here
                    scrapeObj = new Scrape(driver, currentFirm);
                    boolean exist = scrapeObj.waitUntilJSLoaded(2);
                    boolean found;
                    if(exist) {
                        found = scrapeObj.findElementsAndExtractInfo();
                    }else {
                        return exist;
                    }
                    return found;

                }
            };
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Operation in progress", ButtonType.CANCEL);
            alert.setTitle("Loading information");
            alert.setHeaderText("Loading financial data please wait");
            alert.setContentText("Loading...");


            // task.setOnRunning((e) -> alert.showAndWait()); // not used since using alert.initOwner(stage) further down
            task.setOnSucceeded((e) -> {
                alert.close(); // close regardless of what I get back
                try {
                    Boolean returnValue = task.get();
                    if (returnValue) { // check whether successful or not, may do better/thorough checks in Scrape.java
                        data.add(new Overview(
                                scrapeObj.getFirmCode(),
                                scrapeObj.getFirm(),
                                "Loaded successfully",
                                scrapeObj.getInfoMap()
                        ));
                        consoleText.append("New data added: ");
                        consoleText.append(scrapeObj.getFirm());
                        consoleText.append(", "+ "at " + scrapeObj.getDate() + "\n");
                        console.setText(consoleText.toString());
                    } else {
                        data.add(new Overview(
                                addIdentifier.getText(),
                                "",
                                "Error loading data",
                                null
                        ));
                        System.out.println("Something went wrong...");
                        System.out.println(scrapeObj.getInfoMap());
                        // Dialog if data not loaded accurately
                        Alert error = new Alert(Alert.AlertType.ERROR);
                        error.setTitle("Loading information");
                        error.setHeaderText("Something went wrong");
                        error.setContentText("Please try loading data again");
                        error.show();
                    }
                    System.out.println(scrapeObj.getInfoMap());


                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                } catch (ExecutionException e1) {
                    e1.printStackTrace();
                }
                // process return value again in JavaFX thread
            });
            task.setOnFailed((e) -> {
                // eventual error handling by catching exceptions from task.get()
            });
            new Thread(task).start();

            alert.initOwner(stage);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.CANCEL && task.isRunning()) {
                task.cancel();
            }

        });

        hb.getChildren().addAll(addIdentifier, loadButton); //
        hb.setSpacing(3);
        hb_below.getChildren().addAll(deleteButton, infoButton, exportButton);
        hb_below.setSpacing(3);
        hb_console.getChildren().addAll(console);


        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, hb, table, hb_below, hb_console);

        ((Group) scene.getRoot()).getChildren().addAll(vbox);

        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest((event) -> driverController.tearDown());

    }
/*
    class MyIntegerTableCell extends TableCell<Overview, Integer> {

        @Override
        public void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);
            setText(empty ? null : getString());
            setGraphic(null);
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }
*/
    class MyStringTableCell extends TableCell<Overview, String> {

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setText(empty ? null : getString());
            setGraphic(null);
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    class MyEventHandler implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent t) {
            deleteButton.setDisable(false);
            infoButton.setDisable(false);
           /* TableCell c = (TableCell) t.getSource();
            int index = c.getIndex();
            if (index < data.size()) {
                System.out.println("id = " + data.get(index).getIdentifier());
                System.out.println("name = " + data.get(index).getName());
                System.out.println("status = " + data.get(index).getStatus());
                System.out.println("data detail = " + data.get(index).getDataDetail());
            }*/

        }
    }




}
