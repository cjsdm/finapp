package com.cjs.app;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;
import java.util.Map;


public class DriverController {

    String path = "/usr/local/bin/phantomjs";
    WebDriver driver;
    Map<String, String> infoMap = new HashMap<String, String>();

    public DriverController() {
    }

    public DriverController(String pathToPhantomJs) {
        path = pathToPhantomJs;

    }

    public void configureDriver() {
        DesiredCapabilities dCaps;
        dCaps = new DesiredCapabilities();
        dCaps.setJavascriptEnabled(true);
        dCaps.setCapability("takesScreenshot", false);
        dCaps.setCapability(
                PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, path);
        driver = new PhantomJSDriver(dCaps);
    }

    public WebDriver getDriver(){
        return driver;
    }

    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

}
