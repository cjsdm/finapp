package com.cjs.app;

import javafx.beans.property.SimpleMapProperty;
import javafx.beans.property.SimpleStringProperty;
import sun.java2d.pipe.SpanShapeRenderer;

import java.util.Map;

public class Overview {

    private final SimpleStringProperty identifier;
    private final SimpleStringProperty name;
    private final SimpleStringProperty status;
    private final Map<String, Double> dataDetail;

    public Overview(String _identifier, String _name, String _status, Map<String, Double> _dataDetail) {
        this.identifier = new SimpleStringProperty(_identifier);
        this.name = new SimpleStringProperty(_name);
        this.status = new SimpleStringProperty(_status);
        this.dataDetail = _dataDetail;
    }

    public String getIdentifier() {
        return identifier.get();
    }
    public void setIdentifier(String _identifier) {
        identifier.set(_identifier);
    }

    public String getName() {
        return name.get();
    }
    public void setLastName(String _name) {
        name.set(_name);
    }

    public String getStatus() {
        return status.get();
    }
    public void setStatus(String _status) {
        status.set(_status);
    }

    public Map getDataDetail(){
        return dataDetail;
    }


}
