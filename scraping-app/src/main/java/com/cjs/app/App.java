package com.cjs.app;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

import javafx.application.Application;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.openqa.selenium.phantomjs.PhantomJSDriver;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class App {

    public static void main(String[] args) {

        Application.launch(Gui.class, args);

    }

}
