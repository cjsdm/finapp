package com.cjs.app;

import java.util.Date;

public class StockData {

    private double direktavkastning;
    private double pb;
    private double egetKapitalPerAktie;
    private double ps;
    private double omsattning;
    private double vinstPerAktie;
    private double utdelning;
    private String firma;
    private Date date;

    StockData(double _direktavkastning, double _pb, double _egetKapitalPerAktie, double _ps, double _omsattning,
              double _vinstPerAktie, double _utdelning, String _firma, Date _date){

        direktavkastning = _direktavkastning;
        pb = _pb;
        egetKapitalPerAktie = _egetKapitalPerAktie;
        ps = _ps;
        omsattning = _omsattning;
        vinstPerAktie = _vinstPerAktie;
        utdelning = _utdelning;
        firma = _firma;
        date = _date;

    }

    public double getDirektavkastning(){
        return direktavkastning;
    }

    public double getEgetKapitalPerAktie() {
        return egetKapitalPerAktie;
    }

    public double getOmsattning() {
        return omsattning;
    }

    public double getPb() {
        return pb;
    }

    public double getPs() {
        return ps;
    }

    public double getUtdelning() {
        return utdelning;
    }

    public double getVinstPerAktie() {
        return vinstPerAktie;
    }

    public String getFirma() {
        return firma;
    }

    public Date getDate() {
        return date;
    }
}
