package com.cjs.app;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class Scrape {

    WebDriver driver;
    String baseUrl = "http://borsdata.se/";
    String firmCode;
    String firmName;
    Date date;
    Map<String, Double> infoMap = new HashMap<>();

    public Scrape(WebDriver _driver, String _firmCode) {
        driver = _driver;
        firmCode = _firmCode;

    }

    public Boolean waitUntilJSLoaded(int secToWait) {
        WebDriverWait wait = new WebDriverWait(driver, secToWait);
        String url = baseUrl + firmCode + "/nyckeltal";

        driver.get(url);
        By element = By.id("tabs-1");
        try{
            wait.until(presenceOfElementLocated(element));
        }
        catch (TimeoutException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Boolean findElementsAndExtractInfo() {
        date = new Date();
        WebElement rightTopBoxBig;
        List<WebElement> rightTopBoxTop;
        List<WebElement> rightTopBoxDown;

        rightTopBoxBig = driver.findElement(By.xpath("//div[@class='RightTopBoxBig']"));
        rightTopBoxTop = driver.findElements(By.xpath("//div[@class='RightTopBox RightTopBoxTop ng-scope']"));
        rightTopBoxDown = driver.findElements(By.xpath("//div[@class='RightTopBox ng-scope']"));

        if (rightTopBoxBig == null || rightTopBoxTop.isEmpty() || rightTopBoxDown.isEmpty()){
            System.out.println(rightTopBoxBig);
            System.out.println(rightTopBoxTop);
            System.out.println(rightTopBoxDown);
            return false;
        }

        parseTop(rightTopBoxBig);
        parseElements(rightTopBoxTop);
        parseElements(rightTopBoxDown);

        return checkResult();

    }

    private void parseTop(WebElement element) {
        String[] tempArr = element.getText().split("\n");
        if (tempArr.length > 2) {
            firmName = tempArr[0];
            String value = tempArr[1].replaceAll(",", "."); // covert e.g. 0,76 to 0.76
            double price;
            try {
                price = Double.parseDouble(value);
            } catch (NumberFormatException e) {
                price = 0.0; // when no value exist, e.g. "-"
            }
            System.out.println("Pris: " + price);
            infoMap.put("Pris", price);
        }
    }

    private void parseElements(List<WebElement> elements) {
        for (WebElement elem : elements) {
            String[] arr = elem.getText().split("\n");
            if (arr.length > 0) {
                String value = arr[1].replaceAll(",", ".");
                value = value.replaceAll(" %", "");
                double valueToDouble;
                try {
                    valueToDouble = Double.parseDouble(value);
                } catch (NumberFormatException e) {
                    valueToDouble = 0.0; // when no value exist, e.g. "-"
                }

                infoMap.put(arr[0], valueToDouble);
            }
        }
    }

    public Boolean checkResult() {
        if (infoMap.size() == 9) { // 9 key values
            return true;
        }
        return false;
    }

    public Map getInfoMap() {
        return infoMap;
    }

    public String getFirm() {
        return firmName;
    }

    public String getFirmCode() {
        return firmCode;
    }

    public Date getDate() {
        return date;
    }
}
